# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import os.path
from setuptools import setup, find_packages

from epigeec_converter.config import NAME, VERSION, DESCRIPTION

setup(
    name = NAME,
    version = VERSION,
    author = "Simon Hébert-Deschamps",
    author_email = "simon.hebert-deschamps@usherbrooke.ca",
    description = DESCRIPTION,
    packages = find_packages(),
    entry_points = {
        'console_scripts': [
            'epigeec-converter = epigeec_converter.main:main',
        ]
    },
    install_requires = [
        'numpy',
        'scipy',
        'h5py'
    ],
    license = "GPL",
    long_description = DESCRIPTION,
    long_description_content_type = 'text/markdown',
    url = 'https://gitlab.com/labjacquespe/epigeec_converter',
)