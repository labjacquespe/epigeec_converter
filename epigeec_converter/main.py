from __future__ import absolute_import, division, print_function

import sys

from epigeec_converter.parser import ConverterParser

def main(argv=sys.argv[1:]):
    args = ConverterParser.parse_args(argv)
    args.func(args)

if __name__ == '__main__':
    main()