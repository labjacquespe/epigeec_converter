from __future__ import absolute_import, division, print_function

from .hdf5 import Hdf5Converter

class ConverterLauncher(object):
    @staticmethod
    def run_down_resolution(args):
        Hdf5Converter.down_resolution(args.source, args.resolution, args.output)

    @staticmethod
    def run_to_rank(args):
        Hdf5Converter.to_rank(args.source, args.output)
