from __future__ import absolute_import, division, print_function

import sys
import h5py
import shutil
import numpy as np
import scipy.stats

class Hdf5Converter(object):
    @staticmethod
    def down_resolution(source, resolution, output):
        shutil.copy(source, output)
        with EpigeecHdf5Wrapper(output, 'r+') as hdf5_output:
            hdf5_output.reduce_bin(resolution)

    @staticmethod
    def to_rank(source, output):
        shutil.copy(source, output)
        with EpigeecHdf5Wrapper(output, 'r+') as hdf5_output:
            hdf5_output.rank_values()

class EpigeecHdf5Wrapper(object):
    def __init__(self, filename, mode=None):
        self.hdf5 = h5py.File(filename, mode)

    def close(self):
        self.hdf5.close()

    def __enter__(self):
        return self

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        self.close

    @property
    def bin(self):
        return self.hdf5.attrs['bin'][0]

    @bin.setter
    def bin(self, value):
        self.hdf5.attrs['bin'] = np.array([value])

    @property
    def signal_filename(self):
        return self.hdf5.attrs['signal_filename']

    @property
    def datasets(self):
        return self.hdf5[self.signal_filename]

    @property
    def is_rank(self):
        if 'is_rank' not in self.hdf5.attrs:
            return False
        
        return self.hdf5.attrs['is_rank']

    @is_rank.setter
    def is_rank(self, value):
        self.hdf5.attrs['is_rank'] = value

    def reduce_bin(self, new_bin):
        rate = new_bin // self.bin

        if rate == 1:
            return # do nothing
        if self.bin > new_bin:
            raise Exception("The new bin `{}` should be higher than the old one `{}`.".format(new_bin, self.bin))
        if rate % 10 != 0:
            raise Exception("`{}` and `{}` should be divisors of 10.".format(self.bin, new_bin))

        # update bin
        self.bin = new_bin
        for chrname, data in self.datasets.items():
            # recompute data
            missing = (rate - (len(data) % rate)) % rate
            complete = np.append(data, np.zeros(missing))
            newsize = len(complete)//rate

            del self.hdf5[data.name]

            newdata = np.reshape(complete, (newsize, rate)).mean(axis=1)
            data = self.datasets.create_dataset(chrname, data=newdata)
            # sumX
            data.attrs['sumX'] = np.array([np.sum(data)])
            # sumXX
            data.attrs['sumXX'] = np.array([np.square(data).sum()])

    def rank_values(self):
        if self.is_rank:
            return # do nothing

        for data in self.datasets.values():
            n = len(data)
            assert(n < 3024616) #overflow on int64
            data[...] = scipy.stats.rankdata(data, method="ordinal")
            data.attrs['sumX'] = np.array([n * (n + 1) / 2]) #sum of natural numbers
            data.attrs['sumXX'] = np.array([n * (n + 1) * (2 * n + 1) / 6]) #sum of perfect squares
        
