from __future__ import absolute_import, division, print_function
from argparse import ArgumentParser

from .config import NAME, DESCRIPTION, VERSION
from .launcher import ConverterLauncher

class ConverterParser(object):
    @staticmethod
    def parse_args(argv):
        parser = ArgumentParser(prog=NAME, description=DESCRIPTION)
        parser.add_argument('-v', '--version', action='version', version='%(prog)s {0}'.format(VERSION))
        # parser.set_defaults(func=lambda args: parser.print_help())
        subparsers = parser.add_subparsers(help = "Sub-command help.")

        ConverterParser.add_parser(subparsers)

        return parser.parse_args(argv)

    @staticmethod
    def add_parser(subparsers):
        ConverterParser._setup_to_rank_parser(subparsers)
        ConverterParser._setup_down_resolution_parser(subparsers)

    @staticmethod
    def _setup_to_rank_parser(subparsers):
        parser = subparsers.add_parser(
            'to-rank',
            description='Convert the hdf5 with values to ranks.',
            help='Convert the hdf5 with values to ranks.'
        )
        parser.set_defaults(func=ConverterLauncher.run_to_rank)

        parser.add_argument('source', type=str)
        parser.add_argument('output', type=str)

    @staticmethod
    def _setup_down_resolution_parser(subparsers):
        parser = subparsers.add_parser(
            'down-resolution',
            description='Convert the hdf5 file to a lower resolution',
            help='Convert the hdf5 file to a lower resolution'
        )
        parser.set_defaults(func=ConverterLauncher.run_down_resolution)

        parser.add_argument('source', type=str)
        parser.add_argument(
            'resolution',
            choices=[100, 1000, 10000, 100000, 1000000],
            type=int
        )
        parser.add_argument('output', type=str)
